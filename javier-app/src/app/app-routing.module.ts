import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/home-view/home-view.module').then(
        (module) => module.HomeViewModule
      ),
  },
  {
    path: 'about',
    loadChildren: () =>
      import('./pages/about-view/about-view.module').then(
        (module) => module.AboutViewModule
      ),
  },
  {
    path: 'proyects',
    loadChildren: () =>
      import('./pages/proyects-view/proyects-view.module').then(
        (module) => module.ProyectsViewModule
      ),
  },
  {
    path: 'skills',
    loadChildren: () =>
      import('./pages/skills-view/skills-view.module').then(
        (module) => module.SkillsViewModule
      ),
  },
  {
    path: 'contact',
    loadChildren: () =>
      import('./pages/contact-view/contact-view.module').then(
        (module) => module.ContactViewModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
