import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeViewRoutingModule } from './home-view-routing.module';
import { HomePresentationComponent } from './components/home-presentation/home-presentation.component';

import {HttpClientModule} from '@angular/common/http'
import { HomeService } from './services/home.service';

@NgModule({
  declarations: [
    HomePresentationComponent
  ],
  imports: [
    CommonModule,
    HomeViewRoutingModule,
    HttpClientModule
  ],
  providers: [HomeService]
})
export class HomeViewModule { }
