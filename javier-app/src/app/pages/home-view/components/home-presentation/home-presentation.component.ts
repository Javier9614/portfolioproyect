import { Component, OnInit } from '@angular/core';
import { IhomeApi } from '../../models/ihome-view';
import { HomeService } from '../../services/home.service';

@Component({
  selector: 'app-home-presentation',
  templateUrl: './home-presentation.component.html',
  styleUrls: ['./home-presentation.component.scss'],
})
export class HomePresentationComponent implements OnInit {
  public home?: IhomeApi;

  constructor(private readonly homeService: HomeService) {}
  ngOnInit(): void {
    this.getHome();
  }

  public getHome() {
    this.homeService.getHome().subscribe((data: any) => {
      this.home = {
        title: data.title,
        description: data.description,
        img: {
          src: data.img.src,
          alt: data.img.alt
        }
      }
    })
  }
}