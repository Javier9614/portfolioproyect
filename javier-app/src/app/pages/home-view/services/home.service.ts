import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {environment } from '../../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private endpointHome: string = "/home-view";
  constructor(private http: HttpClient) { 
    
  }
  public getHome(){
    return this.http.get(`${environment.baseUrl}${this.endpointHome}`)
  }
}