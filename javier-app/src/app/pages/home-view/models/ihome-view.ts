import { Iimg, IimgApi } from "src/app/models/iapp";

export interface IhomeView {
}

export interface Ihome{
    titleInit: string;
    welcome:string;
    img: Iimg;
}

export interface IhomeApi{
    title: string;
    description:string;
    img: IimgApi;
}