import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePresentationComponent } from './components/home-presentation/home-presentation.component';

const routes: Routes = [
  { path: "", component: HomePresentationComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeViewRoutingModule { }
