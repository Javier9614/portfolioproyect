import { Iimg, IimgApi } from "src/app/models/iapp";


export interface IaboutView {
}

export interface Iabout{
    titleAbout: string;
    descriptionAbout:string;
    imgAbout: Iimg;
}

export interface IaboutViewApi {
    title: string;
    description: string;
    img: IimgApi;
}
