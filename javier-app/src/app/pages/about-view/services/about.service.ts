import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {environment } from '../../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AboutService {
private endpointAbout: string = "/about-view";
  constructor(private http: HttpClient) { 
    
  }
  public getAbout(){
    return this.http.get(`${environment.baseUrl}${this.endpointAbout}`)
  }
}