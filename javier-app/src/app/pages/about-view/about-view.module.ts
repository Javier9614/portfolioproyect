import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutViewRoutingModule } from './about-view-routing.module';
import { AboutMeComponent } from './components/about-me/about-me.component';

import { HttpClientModule } from '@angular/common/http';
import { AboutService } from './services/about.service';


@NgModule({
  declarations: [
    AboutMeComponent
  ],
  imports: [
    CommonModule,
    AboutViewRoutingModule,
    HttpClientModule
  ],
  providers: [AboutService]
})
export class AboutViewModule { }
