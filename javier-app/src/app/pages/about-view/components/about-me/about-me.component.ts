import { Component, OnInit } from '@angular/core';
import { IaboutViewApi } from '../../models/iabout-view';
import { AboutService } from '../../services/about.service';

@Component({
  selector: 'app-about-me',
  templateUrl: './about-me.component.html',
  styleUrls: ['./about-me.component.scss'],
})
export class AboutMeComponent implements OnInit {
  public about?: IaboutViewApi;
  constructor(private readonly aboutService: AboutService) {}

  ngOnInit(): void {
    this.getAbout();
  }
  //Traer la informacion de servicio aqui
  public getAbout() {
    this.aboutService.getAbout().subscribe((data: any) => {
      this.about = {
        title: data.title,
        description: data.description,
        img: {
            src: data.img.src,
            alt: data.img.alt
        }
      }
    })
  }
}
