import { environment } from "../../../../environments/environment";
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SkillsService {
private endpointSkill:string="/skills-view";
  constructor(private http:HttpClient) { }

   public getSkill() {
    return this.http.get(`${environment.baseUrl}${this.endpointSkill}`)}
}
