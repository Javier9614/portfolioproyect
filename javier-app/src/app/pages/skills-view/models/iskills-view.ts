import { IgalleryApi } from "src/app/models/iapp";


export interface IskillsView {
}
export interface IskillApi{
    id: string, 
    img: IgalleryApi;
    description:string;
}
 