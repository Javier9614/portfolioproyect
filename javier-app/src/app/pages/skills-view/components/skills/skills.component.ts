import { Component, Input, OnInit } from '@angular/core';
import { IskillApi } from '../../models/iskills-view';


import { SkillsService } from '../../services/skills.service';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  public skills?: IskillApi[] = [];
  constructor( private readonly skillService:SkillsService) {}

  ngOnInit(): void {
    this.getSkill();
  }
public getSkill() {
  this.skillService.getSkill().subscribe((data: any) =>{
  let auxSkills: IskillApi[] = [];
//console.log(data);
  data.forEach((element: IskillApi) => {
   // console.log("element", element);
    let auxSkill: IskillApi = {
      id: element.id,
      img: element.img,
      description: element.description
      
    }
      //  console.log("lunea 32",auxSkill);
        auxSkills.push(auxSkill);
  })
  this.skills= auxSkills;

});
}
}
