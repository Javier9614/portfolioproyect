import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SkillsViewRoutingModule } from './skills-view-routing.module';
import { SkillsComponent } from './components/skills/skills.component';
import { SkillsService } from './services/skills.service';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    SkillsComponent
  ],
  imports: [
    CommonModule,
    SkillsViewRoutingModule,
    HttpClientModule
  ],
  providers: [ SkillsService]
  
})
export class SkillsViewModule { }
