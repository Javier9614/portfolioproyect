import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactViewRoutingModule } from './contact-view-routing.module';
import { ContactComponent } from './components/contact/contact.component';


@NgModule({
  declarations: [
    ContactComponent
  ],
  imports: [
    CommonModule,
    ContactViewRoutingModule
  ]
})
export class ContactViewModule { }
