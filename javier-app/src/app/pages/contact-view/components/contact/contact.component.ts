import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Icontact } from '../../models/icontact-view';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  @Input() public contact!: Icontact[];

  constructor() {
    this.contact = [
      {
        icon: {
          src: '',
          alt: 'icon',
        },
        text: '¡Chatea conmigo!',
      },

      {
        icon: {
          src: '',
          alt: 'icon',
        },
        text: "'Mandame un email!",
      },

      {
        icon: {
          src: '',
          alt: 'icon',
        },
        text: "'Mandame un email!",
      },

      {
        icon: {
          src: '',
          alt: 'icon',
        },
        text: 'texto1',
        text2: "'Mandame un email!",
      },
    ];
  }

  ngOnInit(): void {}
}
