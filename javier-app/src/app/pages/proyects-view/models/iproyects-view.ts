import { IgalleryApi, Iimg, } from "src/app/models/iapp";

export interface IproyectsView {
}

export interface Iproyects{
    galleryProyects: Igallery[];
}
export interface Igallery{
   titleImgGallery: string;
   img: Iimg;
   descriptions?: string;
}

export interface IproyectsApi{
    id: string, 
    titleImg: string;
    description: string;
    img: IgalleryApi[];
}

