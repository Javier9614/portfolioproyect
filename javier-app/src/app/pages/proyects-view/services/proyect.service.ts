import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { environment } from "../../../../environments/environment";



@Injectable({
  providedIn: 'root'
})
export class ProyectService {
  private endpointProyect :string="/proyect-view";
  constructor(private http:HttpClient) { }
  public getProyect(){
    return this.http.get(`${environment.baseUrl}${this.endpointProyect}`)
  }
}
 