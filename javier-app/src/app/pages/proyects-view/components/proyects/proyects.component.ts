import { Component, OnInit } from '@angular/core';
import { IproyectsApi } from '../../models/iproyects-view';
import { ProyectService } from '../../services/proyect.service';

@Component({
  selector: 'app-proyects',
  templateUrl: './proyects.component.html',
  styleUrls: ['./proyects.component.scss'],
})
export class ProyectsComponent implements OnInit {
  public proyect?: IproyectsApi[] = [];
 
  constructor(private readonly proyectService: ProyectService) {}

  ngOnInit(): void {
    this.getProyect();
  }
  public getProyect() {
    this.proyectService.getProyect().subscribe((data: any) => {
      let auxProyects: IproyectsApi[] = [];

      data.forEach((element: IproyectsApi)=>{
        console.log("description", element.img);
        let auxProyect:IproyectsApi  = {
          id: element.id , 
          titleImg: element.titleImg,
          description: element.description,
          img: element.img,
          
        }
        console.log(auxProyect);
        auxProyects.push(auxProyect);
        
      })
      this.proyect= auxProyects
    });
  }
}
