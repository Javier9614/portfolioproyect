import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProyectsViewRoutingModule } from './proyects-view-routing.module';
import { ProyectsComponent } from './components/proyects/proyects.component';

import { HttpClientModule } from '@angular/common/http';
import { ProyectService } from './services/proyect.service';


@NgModule({
  declarations: [
    ProyectsComponent
  ],
  imports: [
    HttpClientModule,
    CommonModule,
    ProyectsViewRoutingModule
  ],
  providers: [ProyectService]
})
export class ProyectsViewModule { }
