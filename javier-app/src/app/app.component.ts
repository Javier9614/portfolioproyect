import { Component } from '@angular/core';
import { Ifooter, Iheader } from './models/iapp';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public header : Iheader[] = [
    {
      name: "Home",
      navigate:"/home"
    },
    {
      name: "About Me",
      navigate:"/about"
    },
    {
      name: "Proyects",
      navigate:"/proyects"
    },
    {
      name: "Skills",
      navigate:"/skills"
    },
    // {
    //   name: "Contact",
    //   navigate:"/contact"
    // },
  ];

  public footer: Ifooter = {
    rrss: [{
      name: "LinkedIn",
      icon:"",
      link:"",
    },
    {
      name: "GitHub",
      icon:"", 
      link:"",
    }
  ]
  }

};


